package com.magnus.hindibible.versus

class CustomException(message: String) : Exception(message)
